* Completion
  #+begin_src emacs-lisp
  (use-package corfu
    :init
    (global-corfu-mode)
    :config
    (setq corfu-auto t
          tab-always-indent 'complete
          corfu-auto-prefix 2
          corfu-auto-delay 0.25
          corfu-separator ?\s
          corfu-quit-no-match 'separator))

  (use-package dabbrev)

  (use-package kind-icon
    :ensure t
    :after corfu
    :custom
    (kind-icon-default-face 'corfu-default)
    :config
    (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

  (use-package cape
    :bind (("C-c p p" . completion-at-point) ;; capf
           ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
           ("C-c p f" . cape-file)
           ;; ("C-c p h" . cape-history)
           ;; ("C-c p k" . cape-keyword)
           ;; ("C-c p i" . cape-ispell)
           ;; ("C-c p w" . cape-dict)
           ;; ("C-c p s" . cape-symbol)
           ;; ("C-c p l" . cape-line)
           )
    :init
    (add-to-list 'completion-at-point-functions #'cape-file)
    (add-to-list 'completion-at-point-functions #'cape-dabbrev)
    ;;(add-to-list 'completion-at-point-functions #'cape-history)
    ;;(add-to-list 'completion-at-point-functions #'cape-keyword)
    ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
    ;;(add-to-list 'completion-at-point-functions #'cape-dict)
    ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
    ;;(add-to-list 'completion-at-point-functions #'cape-line)
  )
  #+end_src
